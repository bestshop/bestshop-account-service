package io.gary.bestshop.account.controller;

import io.gary.bestshop.account.domain.Account;
import io.gary.bestshop.account.service.AccountService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("accounts")
public class AccountController {

    private AccountService accountService;

    @GetMapping
    public List<Account> getAccounts() {
        return accountService.getAccounts();
    }

    @PostMapping
    public Account createAccount(@RequestBody @Valid Account Account) {
        return accountService.createAccount(Account);
    }

    @PutMapping("{username}")
    public Account updateAccount(@PathVariable("username") String username, @RequestBody @Valid Account Account) {
        return accountService.updateAccount(username, Account);
    }

    @GetMapping("{username}")
    public Account getAccount(@PathVariable("username") String username) {
        return accountService.getAccount(username);
    }

    @DeleteMapping("{username}")
    public void deleteAccount(@PathVariable("username") String username) {
        accountService.deleteAccount(username);
    }

}
