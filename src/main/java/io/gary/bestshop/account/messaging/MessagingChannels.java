package io.gary.bestshop.account.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface MessagingChannels {

    String ACCOUNT_CREATED_OUTPUT = "accountCreatedOutput";

    @Output(ACCOUNT_CREATED_OUTPUT)
    MessageChannel accountCreatedOutput();


    String ACCOUNT_CREATED_INPUT = "accountCreatedInput";

    @Input(ACCOUNT_CREATED_INPUT)
    SubscribableChannel accountCreatedInput();


    String ORDER_COMPLETED_INPUT = "orderCompletedInput";

    @Input(ORDER_COMPLETED_INPUT)
    SubscribableChannel orderCompletedInput();

}
