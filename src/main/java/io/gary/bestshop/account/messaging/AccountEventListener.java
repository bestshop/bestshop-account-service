package io.gary.bestshop.account.messaging;

import io.gary.bestshop.account.service.AccountService;
import io.gary.bestshop.messaging.event.account.AccountCreatedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.Executors;

@Slf4j
@Component
@RequiredArgsConstructor
public class AccountEventListener {

    private final static long ONE_MINUTE = 60L * 1000;

    private final TaskScheduler scheduler = new ConcurrentTaskScheduler(Executors.newSingleThreadScheduledExecutor());

    private final AccountService accountService;

    @StreamListener(MessagingChannels.ACCOUNT_CREATED_INPUT)
    public void handleAccountCreatedEvent(AccountCreatedEvent event) {

        log.info("Processing event: {}", event);

        Date inOneMinute = new Date(System.currentTimeMillis() + ONE_MINUTE);

        scheduler.schedule(() -> accountService.enableAccount(event.getNewAccount().getUsername()), inOneMinute);
    }
}
