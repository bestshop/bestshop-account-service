package io.gary.bestshop.account.messaging;

import io.gary.bestshop.account.domain.Account;
import io.gary.bestshop.messaging.dto.AccountDto;
import io.gary.bestshop.messaging.event.account.AccountCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Component;

import static org.springframework.integration.support.MessageBuilder.withPayload;

@Component
@RequiredArgsConstructor
public class AccountEventPublisher {

    private final MessageChannel accountCreatedOutput;

    public Account publishAccountCreatedEvent(Account account) {
        accountCreatedOutput.send(
                withPayload(new AccountCreatedEvent(toDto(account))).build()
        );
        return account;
    }

    private AccountDto toDto(Account account) {
        return AccountDto.builder()
                .username(account.getUsername())
                .email(account.getEmail())
                .nickname(account.getNickname())
                .firstName(account.getFirstName())
                .lastName(account.getLastName())
                .birthDate(account.getBirthDate())
                .mobilePhone(account.getMobilePhone())
                .createdAt(account.getCreatedAt())
                .lastModifiedAt(account.getLastModifiedAt())
                .build();
    }
}
