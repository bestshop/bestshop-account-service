package io.gary.bestshop.account.errors;

import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@ResponseStatus(NOT_FOUND)
public class AccountNotFoundException extends RuntimeException {

    public AccountNotFoundException(String username) {
        super(String.format("Account not found with username=%s", username));
    }
}
