package io.gary.bestshop.account.service;

import io.gary.bestshop.account.domain.Account;
import io.gary.bestshop.account.errors.AccountAlreadyExistsException;
import io.gary.bestshop.account.errors.AccountNotFoundException;
import io.gary.bestshop.account.messaging.AccountEventPublisher;
import io.gary.bestshop.account.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import static io.gary.bestshop.account.domain.AccountStatus.Enabled;
import static io.gary.bestshop.account.domain.AccountStatus.Pending;
import static java.math.BigDecimal.ZERO;
import static java.util.Optional.ofNullable;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;

    private final AccountEventPublisher accountEventPublisher;

    public List<Account> getAccounts() {

        log.info("Getting accounts");

        return accountRepository.findAll();
    }

    public Account getAccount(@NotNull String username) {

        log.info("Getting account: username={}", username);

        return findByUsernameOrThrow(username);
    }

    public Account createAccount(@NotNull @Valid Account account) {

        log.info("Creating account: account={}", account);

        checkEmailNotExistsOrThrow(account.getEmail());
        checkUsernameNotExistsOrThrow(account.getUsername());

        LocalDateTime now = LocalDateTime.now();
        Account toCreate = account.withStatus(Pending).withCreatedAt(now).withLastModifiedAt(now);

        Account createdAccount = accountRepository.save(toCreate);

        return accountEventPublisher.publishAccountCreatedEvent(createdAccount);
    }

    public Account updateAccount(@NotNull String username, @NotNull @Valid Account account) {

        log.info("Updating account: username={}, account={}", username, account);

        Account existingAccount = findByUsernameOrThrow(username);

        Account toUpdate = existingAccount.withLastModifiedAt(LocalDateTime.now());
        ofNullable(account.getNickname()).ifPresent(toUpdate::setNickname);
        ofNullable(account.getFirstName()).ifPresent(toUpdate::setFirstName);
        ofNullable(account.getLastName()).ifPresent(toUpdate::setLastName);
        ofNullable(account.getMobilePhone()).ifPresent(toUpdate::setMobilePhone);
        ofNullable(account.getBirthDate()).ifPresent(toUpdate::setBirthDate);

        return accountRepository.save(toUpdate);
    }

    public void deleteAccount(@NotNull String username) {

        log.info("Deleting account: username={}", username);

        Account account = findByUsernameOrThrow(username);

        accountRepository.delete(account);
    }

    public void enableAccount(@NotNull String username) {

        log.info("Enabling account: username={}", username);

        Account account = findByUsernameOrThrow(username);

        accountRepository.save(account.withStatus(Enabled));
    }

    public void updatePurchaseStatus(@NotNull String username, @NotNull BigDecimal price) {

        log.info("Updating purchase status: username={}, price={}", username, price);

        Account account = findByUsernameOrThrow(username);

        Integer currentPurchaseCount = ofNullable(account.getPurchaseCount()).orElse(0);
        BigDecimal currentPurchaseAmount = ofNullable(account.getPurchaseAmount()).orElse(ZERO);

        accountRepository.save(account
                .withPurchaseCount(currentPurchaseCount + 1)
                .withPurchaseAmount(currentPurchaseAmount.add(price))
        );
    }

    private void checkUsernameNotExistsOrThrow(String username) {
        if (accountRepository.findByUsername(username).isPresent()) {
            throw new AccountAlreadyExistsException(username);
        }
    }

    private void checkEmailNotExistsOrThrow(String email) {
        if (accountRepository.findByEmail(email).isPresent()) {
            throw new AccountAlreadyExistsException(email);
        }
    }

    private Account findByUsernameOrThrow(String username) {
        return accountRepository.findByUsername(username).orElseThrow(() -> new AccountNotFoundException(username));
    }
}
