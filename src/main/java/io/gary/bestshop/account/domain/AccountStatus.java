package io.gary.bestshop.account.domain;

public enum AccountStatus {

    Pending,
    Disabled,
    Enabled
}
